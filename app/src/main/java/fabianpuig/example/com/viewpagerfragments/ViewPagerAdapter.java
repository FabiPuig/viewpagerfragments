package fabianpuig.example.com.viewpagerfragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Fabian on 08/03/2018.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public int pages = 3;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new Uno();
            case 1:
                return new Dos();
            case 2:
                return new Tres();
            default:
                break;
        }

        return null;
    }


    @Override
    public int getCount() {
        return pages;
    }
}
