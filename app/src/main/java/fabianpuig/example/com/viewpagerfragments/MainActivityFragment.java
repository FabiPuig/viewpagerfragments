package fabianpuig.example.com.viewpagerfragments;

import android.content.Context;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private TabLayout tabLayout;
    private ViewPager viewPager;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        tabLayout = view.findViewById( R.id.tabLayout );
        viewPager = view.findViewById( R.id.viewpager );

        // añadimos el adapter personalizado al ViewPager
        viewPager.setAdapter( new ViewPagerAdapter( getChildFragmentManager() ) );

        // le indicamos al TabLayout que se tiene que coordinar con el ViewPager
        tabLayout.setupWithViewPager( viewPager );
        setTabs();

        // Listener para detectar cambios de página
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                formatTabs();

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return view;
    }

    private void setTabs() {

        // obtenemos el view propio
        View tabview1 = createTabView(getContext(), getString( R.string.title_tab_1 ), getString( R.string.body_tab_1 ) );
        // obtenemos el tab deseado
        TabLayout.Tab tab = tabLayout.getTabAt(0);
        // añadimos la vista al Tab
        tab.setCustomView(tabview1);

        View tabview2 = createTabView(getContext(), getString( R.string.title_tab_2 ), getString( R.string.body_tab_2 ) );
        tab = tabLayout.getTabAt(1);
        tab.setCustomView(tabview2);

        View tabview3 = createTabView(getContext(), getString( R.string.title_tab_3 ), getString( R.string.body_tab_3 ) );
        tab = tabLayout.getTabAt(2);
        tab.setCustomView(tabview3);

        // obtenemos el linear del TabLayout y le ponemos dividers
        View root = tabLayout.getChildAt(0);
        if (root instanceof LinearLayout) {
            ((LinearLayout) root).setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
            ((LinearLayout) root).setDividerDrawable(getResources().getDrawable(R.drawable.tab_divider));
            ((LinearLayout) root).setDividerPadding(20);

        }

    }
    /** Crea el view que ira dentro del tab
     *
     * @param context context
     * @param text1 texto superior del view
     * @param text2 texto inferior del view
     * @return
     */
    private View createTabView(Context context, String text1, String text2) {

        View view = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);

        TextView tv1 = view.findViewById(R.id.tabsTv1);
        TextView tv2 = view.findViewById(R.id.tabsTv2);

        tv1.setText( text1 );
        tv2.setText( text2 );

        return view;
    }

    /** Da formato a los Tab en función de si esta seleccionado o no
     *
     */
    private void formatTabs(){

        for(int i = 0; i < ((ViewPagerAdapter) viewPager.getAdapter() ).pages; i++)
        {
            View tabView = ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(i);
            tabView.requestLayout();

            TextView tv = tabView.findViewById( R.id.tabsTv1 );
            TextView tv2 = tabView.findViewById( R.id.tabsTv2 );


            if (Build.VERSION.SDK_INT < 23) {

                //pone estilo en TextView de los tabs dependiendo si esta seleccionado o no
                if( i == tabLayout.getSelectedTabPosition() ){
                    tv.setTextAppearance( getContext(), R.style.boldText );
                    tv2.setTextAppearance( getContext(), R.style.boldText );
                }else{
                    tv.setTextAppearance( getContext(), R.style.normalText );
                    tv2.setTextAppearance( getContext(), R.style.normalText );
                }

            } else{

                if( i == tabLayout.getSelectedTabPosition() ){
                    tv.setTextAppearance( R.style.boldText );
                    tv2.setTextAppearance( R.style.boldText );
                }else{
                    tv.setTextAppearance( R.style.normalText );
                    tv2.setTextAppearance( R.style.normalText );
                }

            }
        }
    }
}
